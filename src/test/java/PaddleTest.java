import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.Color;

import org.junit.jupiter.api.Test;
/**
 * 
 * @author imad
 * Classe de test de la classe Paddle
 *
 */
public class PaddleTest {
	/**
	 * Tester l'intersection de la balle et le paddle
	 */
	
	@Test
	void test_move_up(){
		Ball b = new Ball(Color.BLACK,2,2);
		Paddle p = new Paddle(2,12);
		p.moveUp();
		Boolean bool = p.contains(b);
		assertTrue(bool);
	}
	
	@Test
	void test_move_down(){
		Ball b = new Ball(Color.BLACK,2,2);
		Paddle p = new Paddle(2,-8);
		p.moveDown();
		Boolean bool = p.contains(b);
		assertTrue(bool);
	}
	
	@Test
	void test_move_to_case1(){
		Ball b = new Ball(Color.BLACK,2,2);
		Paddle p = new Paddle(2,2);
		p.moveTo(4);
		Boolean bool = p.contains(b);
		assertTrue(bool);
	}
	
	@Test
	void test_move_to_case2(){
		Ball b = new Ball(Color.BLACK,2,12);
		Paddle p = new Paddle(2,2);
		p.moveTo(14);
		Boolean bool = p.contains(b);
		assertTrue(bool);
	}
	
	@Test
	void test_move_to_case3(){
		Ball b = new Ball(Color.BLACK,2,-8);
		Paddle p = new Paddle(2,2);
		p.moveTo(-14);
		Boolean bool = p.contains(b);
		assertTrue(bool);
	}
	
	@Test
	void test_move_case1(){
		Ball b = new Ball(Color.BLACK,2,0);
		Paddle p = new Paddle(2,2);
		p.move(-4);
		Boolean bool = p.contains(b);
		assertTrue(bool);
	}
	
	@Test
	void test_move_case2(){
		Ball b = new Ball(Color.BLACK,2,300);
		Paddle p = new Paddle(2,2);
		p.move(300);
		Boolean bool = p.contains(b);
		assertTrue(bool);
	}
	
	@Test
	void test_move_case3 (){
		Ball b = new Ball(Color.BLACK,2,2);
		Paddle p = new Paddle(2,0);
		p.move(2);
		Boolean bool = p.contains(b);
		assertTrue(bool);
	}
	
	
}
